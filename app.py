import random
from decimal import Decimal

FIELD_SIZE = 10 ** 5


def reconstruct_secret(shares):
    """
    Combines individual shares (points on graph)
    using Lagranges interpolation.

    `shares` is a list of points (x, y) belonging to a
    polynomial with a constant of our key.
    """
    sums = 0
    for j, share_j in enumerate(shares):
        xj, yj = share_j
        prod = Decimal(1)

        for i, share_i in enumerate(shares):
            xi, _ = share_i
            if i != j:
                prod *= Decimal(Decimal(xi) / (xi - xj))

        prod *= yj
        sums += Decimal(prod)

    return int(round(Decimal(sums), 0))


def polynom(x, coefficients):
    """
    This generates a single point on the graph of given polynomial
    in `x`. The polynomial is given by the list of `coefficients`.
    """
    point = 0
    for coefficient_index, coefficient_value in enumerate(coefficients[::-1]):
        point += x ** coefficient_index * coefficient_value
    return point


def coeff(t, secret_key):
    """
    Randomly generate a list of coefficients for a polynomial with
    degree of `t` - 1, whose constant is `secret`.

    For example with a 3rd degree coefficient like this:
            3x^3 + 4x^2 + 18x + 554

            554 is the secret, and the polynomial degree + 1 is
            how many points are needed to recover this secret.
            (in this case it's 4 points).
    """
    coeff = [random.randrange(0, FIELD_SIZE) for _ in range(t - 1)]
    coeff.append(secret_key)
    return coeff


def generate_shares(n, m, secret_key):
    """
    Split given `secret` into `n` shares with minimum threshold
    of `m` shares to recover this `secret`, using SSS algorithm.
    """
    coefficients = coeff(m, secret_key)
    shares = []

    for i in range(1, n + 1):
        x = random.randrange(1, FIELD_SIZE)
        shares.append((x, polynom(x, coefficients)))

    return shares


if __name__ == "__main__":

    # (3,5) shamir scheme
    n = 5
    t = 3
    secret = int(input("secret:\n"))
    print(f"Original Secret: {secret}")

    # Phase I: Generation of shares
    shares = generate_shares(n, t, secret)
    print(
        f'Shares: {", ".join(f" number {i} = " + str(share) for i,share in enumerate(shares))}'
    )
    pool = []
    for _ in range(t):
        i = int(input("enter an index number: \n"))
        pool.append(shares[i])
    # Phase II: Secret Reconstruction
    # Picking t shares randomly for
    # reconstruction
    print(f'Combining shares: {", ".join(str(share) for share in pool)}')
    print(f"Reconstructed secret: {reconstruct_secret(pool)}")
    assert secret == reconstruct_secret(pool)
